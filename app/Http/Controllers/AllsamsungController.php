<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class AllsamsungController extends Controller
{
    public function index(Request $request)
    {
        $data['samsung'] = DB::table('samsung')
            ->where('active', 1)
            ->get();
        return view('samsung.index', $data);
    }
}
