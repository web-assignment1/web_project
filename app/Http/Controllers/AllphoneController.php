<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class AllphoneController extends Controller
{
    public function index(Request $request)
    {
        $data['iphone'] =  DB::table('iphone')
            ->where('active', 1)
            ->get();
        return view('iPhone.index', $data);
    }
}
