<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class VivoController extends Controller
{
    public function index()
    {
        $data['vivo'] = DB::table('vivo')
            ->where('active', 1)
            ->get();
        return view('vivo.index', $data);
    }
}
