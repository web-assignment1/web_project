<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    public function index()
    {
        $data['iphone'] = DB::table('iphone')
            ->where('active', 1)->take(8)
            ->get();
        $data['samsung'] = DB::table('samsung')->take(8)
            ->where('active', 1)->get();
        $data['vivo'] = DB::table('vivo')->take(4)
            ->where('active', 1)
            ->get();
        $data['oppo'] = DB::table('oppo')->take(4)
            ->where('active', 1)
            ->get();
        $data['secondhand'] = DB::table('secondhand')->take(5)
            ->get();
        return view('index', $data);
    }
}
