<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
class DetailsController extends Controller
{   
    public function iphone(Request $request)
    {   
        $id = $request->id;
        $data['details'] = DB::table('iphone')->where('id', $id)
            ->where('active', 1)->first();
       return view('details.iphone', $data);
    }
    
    public function samsung(Request $request)
    {
        $id = $request->id;
        $data['details'] = DB::table('samsung')->where('id', $id)
            ->where('active', 1)->first();
        return view('details.samsung', $data);
    }

    public function oppo(Request $request)
    {
        $id = $request->id;
        $data['details'] = DB::table('oppo')->where('id', $id)
            ->where('active', 1)->first();
        return view('details.oppo', $data);
    }

    public function vivo(Request $request)
    {
        $id = $request->id;
        $data['details'] = DB::table('vivo')->where('id', $id)
            ->where('active', 1)->first();
        return view('details.vivo', $data); 
    }

    public function secondhand(Request $request)
    {
        $id = $request->id;
        $data['details'] = DB::table('secondhand')->where('id', $id)
            ->where('active', 1)->first();
        return view('details.secondhand', $data);
    }

    public function specialoffer(Request $request)
    {
        $id = $request->id;
        $data['details'] = DB::table('specialoffer')->where('id', $id)
            ->where('active', 1)->first();
        return view('details.specialoffer', $data);
    }

}
