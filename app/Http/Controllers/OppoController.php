<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
class OppoController extends Controller
{
    public function index()
    {
        $data['oppo'] = DB::table('oppo')
            ->where('active', 1)
            ->get();
        return view('oppo.index', $data);
    }
}
