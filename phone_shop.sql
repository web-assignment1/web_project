-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 13, 2022 at 03:53 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phone_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `logo`, `description`, `phone`, `email`, `address`, `created_at`) VALUES
(1, 'Delta-Plc', 'uploads/companies/vHKy2ThcZaBHidF6bMiPQ5w6hmDB9AqpFQTuLyFH.png', '“DELTA” is one of Cambodia’s microfinance institution (“MFI”) founded in 2013 by a local experienced team with strong backgrounds on business management, financial services and banking, private equity and real estate …etc as the Delta Microfinance PLC.', '081 777 025, 081 777 155', 'info@delta-plc.com', 'No 546, street 1982, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia.', '2022-07-28 08:30:24');

-- --------------------------------------------------------

--
-- Table structure for table `iphone`
--

DROP TABLE IF EXISTS `iphone`;
CREATE TABLE IF NOT EXISTS `iphone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `d_image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `iphone`
--

INSERT INTO `iphone` (`id`, `title`, `photo`, `description`, `price`, `date`, `active`, `created_at`, `d_image`) VALUES
(1, 'iPhone 13 256GB', 'assets/img/iphone/1.png', NULL, '$873', NULL, 1, '2022-10-27 03:15:51', NULL),
(2, 'iPhone 12 Pro 256G', 'assets/img/iphone/2.jpg', NULL, '$1500', NULL, 1, '2022-10-27 03:40:47', NULL),
(3, 'iPhone 12 Pro 512G', 'assets/img/iphone/3.png', NULL, '$1741', NULL, 1, '2022-10-27 03:40:47', NULL),
(4, 'iPhone 12 Mini 64G', 'assets/img/iphone/4.jpg', NULL, '$639', NULL, 1, '2022-10-27 03:41:40', NULL),
(5, 'iPhone 12 Mini 128G', 'assets/img/iphone/5.png', NULL, '$669', NULL, 1, '2022-10-27 03:45:16', NULL),
(6, 'iPhone 12 Pro 512G', 'assets/img/iphone/8.jpg', NULL, '$1620', NULL, 1, '2022-10-27 03:45:16', NULL),
(7, 'iPhone 12 Pro 128G', 'assets/img/iphone/9.jpg', NULL, '$1258', NULL, 1, '2022-10-27 03:52:56', NULL),
(8, 'iPhone 12 Mini 128G', 'assets/img/iphone/8.jpg', NULL, '$819', NULL, 1, '2022-10-27 03:52:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `line` int(11) NOT NULL DEFAULT '1',
  `qty` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` date DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_updated` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `line`, `qty`, `active`, `created_at`, `updated_at`, `user_updated`) VALUES
(1, 'Banlung', 1, 0, 1, NULL, '2019-09-08 13:00:12', NULL),
(2, 'Banteay Meanchey', 2, 0, 1, NULL, '2019-09-08 13:00:46', NULL),
(3, 'Battambang', 3, 0, 1, NULL, '2019-09-08 13:01:01', NULL),
(4, 'Bavet', 1, 0, 1, NULL, '2019-09-08 13:01:17', NULL),
(5, 'Kampong Cham', 2, 0, 1, NULL, '2019-09-08 13:01:29', NULL),
(6, 'Kampong Chhnang', 3, 0, 1, NULL, '2019-09-08 13:01:42', NULL),
(7, 'Kampong Speu', 1, 0, 1, NULL, '2019-09-08 13:01:57', NULL),
(8, 'Kampong Thom', 2, 0, 1, NULL, '2019-09-08 13:02:08', NULL),
(9, 'Kampot', 3, 0, 1, NULL, '2019-09-08 13:02:20', NULL),
(10, 'Kandal', 1, 0, 1, NULL, '2019-09-08 13:02:35', NULL),
(11, 'Kep', 2, 0, 1, NULL, '2019-09-08 13:02:47', NULL),
(12, 'Koh Kong', 3, 0, 1, NULL, '2019-09-08 13:03:22', NULL),
(13, 'Kratie', 1, 0, 1, NULL, '2019-09-08 13:03:34', NULL),
(14, 'Mondulkiri', 2, 0, 1, NULL, '2019-09-08 13:04:07', NULL),
(15, 'Oddor Meanchey', 3, 0, 1, NULL, '2019-09-08 13:04:27', NULL),
(16, 'Overseas', 1, 0, 1, NULL, '2019-09-08 13:04:46', NULL),
(17, 'Pailin', 2, 0, 1, NULL, '2019-09-08 13:05:00', NULL),
(18, 'Phnom Penh', 3, 85, 1, NULL, '2019-09-08 13:05:12', NULL),
(19, 'Poipet', 1, 0, 1, NULL, '2019-09-08 13:05:24', NULL),
(20, 'Preah Sihanouk', 2, 0, 1, NULL, '2019-09-08 13:05:36', NULL),
(21, 'Preah Vihear', 3, 0, 1, NULL, '2019-09-08 13:05:47', NULL),
(22, 'Prey Veng', 1, 0, 1, NULL, '2019-09-08 13:05:59', NULL),
(23, 'Provinces', 2, 0, 1, NULL, '2019-09-08 13:06:12', NULL),
(24, 'Pursat', 3, 0, 1, NULL, '2019-09-08 13:06:26', NULL),
(25, 'Rattanakiri', 1, 0, 1, NULL, '2019-09-08 13:06:39', NULL),
(26, 'Siem Reap', 2, 0, 1, NULL, '2019-09-08 13:06:54', NULL),
(27, 'Sre Ambel', 3, 0, 1, NULL, '2019-09-08 13:07:08', NULL),
(28, 'Stung Treng', 1, 0, 1, NULL, '2019-09-08 13:07:19', NULL),
(29, 'Svay Rieng', 2, 0, 1, NULL, '2019-09-08 13:07:32', NULL),
(30, 'Takeo', 3, 2, 1, NULL, '2019-09-08 13:07:45', NULL),
(31, 'Takhmao', 1, 0, 1, NULL, '2019-09-08 13:07:56', NULL),
(32, 'Tboung Khmum', 2, 0, 1, NULL, '2019-09-08 13:08:07', NULL),
(33, '12333', 1, 0, 0, '2019-10-16', '2019-10-16 13:40:54', 1),
(34, '23', 2, 0, 0, '2019-10-16', '2019-10-16 13:40:59', 1),
(35, '23', 3, 0, 0, '2019-10-16', '2019-10-16 13:41:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `photo`, `description`, `active`) VALUES
(1, 'Apple\'s iPhone 14 Event: Everything Announced', 'assets/img/news/1.jpg', 'Apple just finished holding its annual iPhone event and that of course meant new iPhones. There were plenty of surprised with a larger iPhone 14 Plus replacing last year\'s mini model and a pair of extremely well equipped iPhone Pro models. Though, the biggest surprise was perhaps the Apple Watch Ultra, which is basically Apple\'s wearable on steroids with a bigger screen and an extremely rugged build. In case you missed it we\'ve rounded up every major product announcement you need to know about', 1),
(2, 'Samsung Unpacked Event Recap: Every Announcement You May Have Missed', 'assets/img/news/2.webp', 'Samsung hosted its semiannual Unpacked event on Wednesday, and as we\'ve come to expect from the company, its August 2022 announcements delivered news about its latest phones, watches and earbuds: the Galaxy Z Fold 4 and Z Flip 4, Galaxy Watch 5 and Watch 5 Pro and Galaxy Buds 2 Pro. The Galaxy Z Flip 3 will get one of our favorite last-gen perks, a $100 price cut.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oppo`
--

DROP TABLE IF EXISTS `oppo`;
CREATE TABLE IF NOT EXISTS `oppo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oppo`
--

INSERT INTO `oppo` (`id`, `title`, `photo`, `price`, `active`, `created_at`) VALUES
(1, 'Oppo Reno8 4G', 'assets/img/oppo/1.png', '$357', 1, '2022-10-27 09:30:07'),
(2, 'OPPO Reno7 4G', 'assets/img/oppo/2.png', '$369', 1, '2022-10-27 09:30:07'),
(3, 'OPPO A96', 'assets/img/oppo/3.png', '$269', 1, '2022-10-27 09:30:07'),
(4, 'Oppo Find X5 Pro', 'assets/img/oppo/4.png', '$1199', 1, '2022-10-27 09:30:07');

-- --------------------------------------------------------

--
-- Table structure for table `samsung`
--

DROP TABLE IF EXISTS `samsung`;
CREATE TABLE IF NOT EXISTS `samsung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `samsung`
--

INSERT INTO `samsung` (`id`, `title`, `photo`, `price`, `active`, `created_at`) VALUES
(1, 'Samsung Galaxy S22 Ultra 12GB 512GB', 'assets/img/samsung/1.png', '$1399', 1, '2022-10-27 04:41:13'),
(2, 'Samsung Galaxy Z Fold4 512GB', 'assets/img/samsung/2.png', '$1899', 1, '2022-10-27 04:41:13'),
(4, 'Samsung Galaxy Z Flip4 256GB', 'assets/img/samsung/3.png', '$1099', 1, '2022-10-27 04:55:17'),
(5, 'Samsung Galaxy S22 8GB 128GB', 'assets/img/samsung/4.png', '$849', 1, '2022-10-27 04:55:17'),
(6, 'Samsung Galaxy S22 8GB 256GB', 'assets/img/samsung/5.png', '$899', 1, '2022-10-27 04:55:17'),
(7, 'Samsung Galaxy S22 Plus 8GB 128GB', 'assets/img/samsung/6.png', '$999', 1, '2022-10-27 04:55:17'),
(8, 'Samsung Galaxy S22 Plus 8GB 256GB', 'assets/img/samsung/7.png', '$1049', 1, '2022-10-27 04:55:17'),
(9, 'Samsung Galaxy S22 Ultra 12GB 256GB', 'assets/img/samsung/8.png', '$1299', 1, '2022-10-27 04:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `secondhand`
--

DROP TABLE IF EXISTS `secondhand`;
CREATE TABLE IF NOT EXISTS `secondhand` (
  `id` int(11) NOT NULL,
  `title` varchar(25) COLLATE utf32_unicode_ci NOT NULL,
  `photo` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `de_photo` varchar(250) COLLATE utf32_unicode_ci DEFAULT NULL,
  `price` varchar(25) COLLATE utf32_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `secondhand`
--

INSERT INTO `secondhand` (`id`, `title`, `photo`, `de_photo`, `price`, `active`) VALUES
(1, 'iPhone 13 Pro', 'assets/img/secondhand/1.jpg', 'assets/img/details/1.png', '$ 950', 1),
(2, 'Samsung Galaxy S22 Ultra', 'assets/img/secondhand/2.webp', 'assets/img/details/5.png', '$ 1,000', 1),
(3, 'iPhone 13', 'assets/img/secondhand/3.jpg', 'assets/img/details/1.png', '$ 900', 1),
(4, 'Pixel 4a', 'assets/img/secondhand/4.jpg', 'assets/img/details/4.jpg', '$ 200', 1),
(5, 'Samsung Galaxy Fold 4', 'assets/img/secondhand/5.webp', NULL, '$ 1,100', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` tinyint(4) DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

DROP TABLE IF EXISTS `slides`;
CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `photo`, `sequence`, `active`, `created_at`) VALUES
(1, 'uploads/slides/J1h43V6HsgdMpB2IVN6Iivi610MohcCOTtBCx1o9.jpg', 1, 1, '2022-08-05 04:24:50'),
(2, 'uploads/slides/Jida2WgOKCrpkaoQDucG5T8rj1vqjSKJ3RzlTDOO.jpg', 1, 1, '2022-08-05 04:28:53'),
(3, 'uploads/slides/aBVCJn2vlh57CfMVoSUfIlzkIYJFn9tghatK2fXL.jpg', 1, 1, '2022-08-05 04:29:29'),
(4, 'uploads/slides/W1CTyOqNOVFk0NSZhOT2dRAC2Fnvzig11X9I68wK.jpg', 1, 1, '2022-08-05 04:29:34'),
(5, 'uploads/slides/pfCXWU65RSSg0ln7KxUTReI5uq04VHqdJ9ZQwpQq.jpg', 1, 0, '2022-08-05 04:29:52'),
(6, 'uploads/slides/F39Jdv1HSRb9e8OdvUzd5jjFzuNuNpLeLbCM4DzO.jpg', 1, 1, '2022-09-21 08:56:13');

-- --------------------------------------------------------

--
-- Table structure for table `socials`
--

DROP TABLE IF EXISTS `socials`;
CREATE TABLE IF NOT EXISTS `socials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `socials`
--

INSERT INTO `socials` (`id`, `name`, `icon`, `url`, `active`, `created_at`) VALUES
(1, 'Facebook', 'facebook', 'https://web.facebook.com/deltaplc', 1, '2022-07-29 07:51:43'),
(2, 'YouTube', 'youtube', 'https://www.youtube.com/channel/UCsjWn-uG90XP2QpBIQj5ZZg/featured', 0, '2022-07-29 10:02:04'),
(3, 'Twitter', 'twitter', 'https://www.twitter.com/deltaplc1', 1, '2022-07-29 14:19:57'),
(4, 'Instagram', 'instagram', 'https://web.instagram.com/deltaplc', 1, '2022-07-29 14:20:50'),
(5, 'Linkedin', 'linkedin', 'https://web.linkedin.com/deltaplc', 1, '2022-07-29 14:21:39');

-- --------------------------------------------------------

--
-- Table structure for table `specialoffer`
--

DROP TABLE IF EXISTS `specialoffer`;
CREATE TABLE IF NOT EXISTS `specialoffer` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(50) COLLATE utf32_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf32_unicode_ci DEFAULT NULL,
  `price` varchar(25) COLLATE utf32_unicode_ci DEFAULT NULL,
  `discount` varchar(25) COLLATE utf32_unicode_ci DEFAULT NULL,
  `finalprice` varchar(25) COLLATE utf32_unicode_ci DEFAULT NULL,
  `deadline` varchar(25) COLLATE utf32_unicode_ci DEFAULT NULL,
  `active` char(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Dumping data for table `specialoffer`
--

INSERT INTO `specialoffer` (`id`, `title`, `photo`, `price`, `discount`, `finalprice`, `deadline`, `active`) VALUES
(1, 'iPhone 14 Pro', 'assets/img/specialoffer/1.jpg', '$1,279', '$60 OFF', '$1,219', 'Ends on 12 Dec 2022', NULL),
(2, 'Galaxy S22 Ultra', 'assets/img/specialoffer/2.png', '$1,299', '$150 OFF', '$1,149', 'Ends on 25 Dec 2022', NULL),
(3, 'Galaxy Z Flip 3 5G', 'assets/img/specialoffer/3.png', '$849', '$69 OFF', '$780', 'Ends on 20 Nov 2022', NULL),
(4, 'iPhone 13 Pro Max', 'assets/img/specialoffer/4.jpg', '$1,189', '$110 OFF', '$1,079', 'Ends on 20 Nov 2022', NULL),
(5, 'iPhone 13 Pro', 'assets/img/specialoffer/5.png', '$1,099', '$110 OFF', '$989', 'Ends on 20 Nov 2022', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `photo`, `created_at`, `active`, `updated_at`, `username`) VALUES
(17, 'Eour Mon', 'eourmon@gmail.com', NULL, '$2y$10$E1Kmb9J7MnJxsFb55P1v7OdCfNxdcge5iV9zpVGmnc6ARs9pfD9fO', NULL, 'uploads/users/nwlXMUMbL910UzQLBZ6j4KAVKngXCFptkIJUDZ75.jpg', '2021-01-29 03:27:56', 1, NULL, 'eourmon'),
(12, 'Admin', 'admin@gmail.com', NULL, '$2y$10$7ONANegBXtT0jQFf7lV/z.guYfSU1Q2pXAsPRQT937.bh8s1Ao9hC', 'AucaeOlwKpgtARUUBJxeJFfzv8faaSlJULtqouxv8ozs6KOyoCgb4ecEQNka', 'uploads/users/P33mnMNUFJDWIhrvcXDOXJaNO1PQmWPdCV4JQrjQ.png', NULL, 1, NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `vivo`
--

DROP TABLE IF EXISTS `vivo`;
CREATE TABLE IF NOT EXISTS `vivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vivo`
--

INSERT INTO `vivo` (`id`, `title`, `photo`, `price`, `active`, `created_at`) VALUES
(1, 'vivo V23e 128GB', 'assets/img/vivo/1.png', '$329', 1, '2022-10-27 09:16:20'),
(2, 'vivo V23e 256GB', 'assets/img/vivo/2.png', '$359', 1, '2022-10-27 09:16:20'),
(3, 'Vivo Y20s', 'assets/img/vivo/3.jpg', '$199', 1, '2022-10-27 09:16:20'),
(4, 'Vivo Y50', 'assets/img/vivo/4.png', '$249', 1, '2022-10-27 09:16:20'),
(5, 'Vivo Y22s', 'assets/img/vivo/5.png', '$199', 1, '2022-10-28 05:00:27');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
