<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PhoneStore</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Logo -->
  <link href="{{asset('assets/img/logo/logo.png')}}" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('assets/fontawesome/css/all.min.css')}}">

  <!-- Main CSS File -->
  <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/owl.theme.default.css')}}">

  <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/google_pixel.css')}}" rel="stylesheet">
  <link href="{{asset('assets/css/aboutus.css')}}" rel="stylesheet">
  <!-- ============================================================== -->
  @yield('css')
</head>

<body class="body-color">
  <!-- Section Header -->
  <section class="top-bar">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="logo">
            <a href="{{url('/')}}"><img src="{{asset('assets/img/logo/logo.png')}}" alt=""></a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-2">
              <div class="header-icons">
                <ul>
                  <li>
                    <a href="#"><i class="bi bi-phone"></i></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="header-contact">
                <div class="list-contact">
                  <a href="">(+855) 010 535 666</a><br>
                  <a href="">(+855) 010 535 666</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="row">
            <div class="col-sm-2">
              <div class="header-icons">
                <ul>
                  <li>
                    <a href="#"><i class="bi bi-pin-map-fill"></i></a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-10">
              <div class="header-contact">
                <div class="list-contact">
                  <a href="{{asset('https://www.google.com/maps?ll=11.553045,104.90225&z=12&t=m&hl=en&gl=KH&mapclient=embed&cid=11405242764460484188')}}">#Chenla Theatre (Next to Canadia Bank Chenla), Blvd Mao Tse Toung, Sangkat Psar Derm Ko, Khan Tuol Kouk, Phnom Penh.</a><br>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Section Header -->

  <!-- Section Menu -->
  <section class="menu_bar sticky-top">
    <nav class="navbar navbar-expand-lg menu_bar sticky-top pd-5">
      <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 col-lg-2 col-md-2 col-sm-4">
            <li class="nav-items">
              <a class="nav-link active" href="{{url('/')}}">Home</a>
            </li>
            <li class="nav-items dropdown">
              
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Smart phone
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="{{url('iPhone')}}">iPhone</a></li>
                <li><a class="dropdown-item" href="{{url('samsung')}}">Samsung</a></li>
                <li><a class="dropdown-item" href="#">Oppo</a></li>
                <li><a class="dropdown-item" href="">Vivo</a></li>
                <li><a class="dropdown-item" href="{{url('google_pixel')}}">Google Pixel</a></li>
              </ul>
            </li>
            <li class="nav-items">
              <a href="{{url('second_hand_product')}}" class="nav-link">Secondhand</a>
            </li>
            <li class="nav-items">
              <a href="{{url('special_offer')}}" class="nav-link">SpecialOffer</a>
            </li>
            <li class="nav-items">
              <a href="{{url('news')}}" class="nav-link">News</a>
            </li>
            <li class="nav-items">
              <a class="nav-link" href="{{url('aboutus')}}">AboutUs</a>
            </li>
            <li class="nav-items">
              <a href="{{url('contact')}}" class="nav-link">ContactUs</a>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          </form>
        </div>
      </div>
    </nav>
  </section>
  <!-- End Section Menu -->
  @yield('slide')
  
  <main id="main">
  
  <div class="container">
    @yield('content')
  </div>
  
  </main>

  <!-- ======= Footer ======= -->
  <footer>
    <div class="footer-area">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="footer-content">
              <div class="footer-head">
                <div class="footer-logo">
                  <img src="{{asset('assets/img/logo/logo1.png')}}" alt="" width="120">
                </div>
                <p>
                  Ary Store Phone Shop, available for Sale | Buy | Installment for all kinds of new and used phones, tablets, smart watches, electronic devices, accessories and computers.
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Direct Contact</h4>
                <div class="footer-contacts">
                  <p class="contact"> <a href=""><i class="fa fa-phone icons"></i> (+855) 010 535 666</a></p>
                  <p class="contact"> <a href=""><i class="fa fa-phone icons"></i> (+855) 010 535 666</a></p>
                  <p class="contact"> <a href=""><i class="fa fa-envelope icons"></i> phonestore@gmail.com</a></p>
                  <p class="contact"> <a href="https://t.me/phonestore"><i class="fab fa-telegram-plane icons"></i> https://t.me/phonestore</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Find Us on social</h4>
                <div class="footer-icons">
                  <ul>
                    <li>
                      <a href="#"><i class="bi bi-facebook"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="bi bi-twitter"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="bi bi-instagram"></i></a>
                    </li>
                    <li>
                      <a href="#"><i class="bi bi-line"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="footer-content">
              <div class="footer-head">
                <h4>Our Shop Location</h4>
                <p><i class="bi bi-map"></i>
                Chenla Theatre (Next to Canadia Bank Chenla), Blvd Mao Tse Toung, Sangkat Psar Derm Ko, Khan Tuol Kouk, Phnom Penh.
                </p>
                <div style="width: 50px;">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15635.932509835216!2d104.9022755!3d11.5530671!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9e478fdde0c2e65c!2sAry%20Store%20Phone%20Shop!5e0!3m2!1sen!2skh!4v1663052580116!5m2!1sen!2skh" width="600" height="120" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-area-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="copyright text-center">
              <p>
                &copy; <?php echo date("Y") ?>. <a href="{{url('/')}}"></a>. All Rights Reserved
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- End  Footer -->

  <div id="preloader"></div>
  <!-- <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a> -->

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/js/main.js')}}"></script>
  <script src="{{asset('assets/js/jquery.min.js')}}"></script>
  <!-- Jquery slide -->
  <script>
    $(document).ready(function(){
      // slide
      var slide = $('.slide');
        
        slide.hide();
        var x = 0;
        var numSlide = slide.length;
        slide.eq(x).show();
        get_page_slide();
        // slide next
        $('.btn-next').click(function(){
            slide.eq(x).hide(x);
            $('.page-slide').find('ul li').eq(x).removeClass('active-slide');
            x++;
            if(x>numSlide){
                x = 0;
            }
            slide.eq(x).show();
            $('.page-slide').find('ul li').eq(x).addClass('active-slide');
        });

        // slide back
        $('.btn-back').click(function(){
            slide.eq(x).hide();
            $('.page-slide').find('ul li').eq(x).removeClass('active-slide');
            if(x == 0){
                x = numSlide;
            }
            x--;
            slide.eq(x).show();
            $('.page-slide').find('ul li').eq(x).addClass('active-slide');
        });

        // get pagination
        function get_page_slide(){
            var li = '';
            var x = 0;
            for( i=0; i<numSlide; i++){
                x++;
                li += '<li>'+x+'</li>';
            }
            $('.page-slide').find('ul').html(li);
            $('.page-slide').find('ul li').eq(0).addClass('active-slide');
        }

        // slide with page slide
        $('.page-slide').on('click', 'ul li', function(){
            slide.eq(x).hide();
            $('.page-slide').find('ul li').eq(x).removeClass('active-slide');
            x = $(this).index();
            slide.eq(x).show();
            $('.page-slide').find('ul li').eq(x).addClass('active-slide');
            
        });
    });
  </script>
@yield('js')

</body>

</html>