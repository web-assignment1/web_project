@extends('layouts.master')
@section('content')
<!-- Section Samsung -->
    <div class="container">
        <div class="row body-box_iphon_detail">
            <div class="col-sm-12 mb-4">
                <div class="row">
                        <div class="link_title">
                    <h2 style="color:#c14794;">Samsung</h2>
                </div>
                <div class="row mt-4 mb-5">
                    @foreach($samsung as $s)
                    <div class="col-sm-3 Wrp_product">
                        <a href="{{url('details/samsung?id='. $s->id)}}">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset($s->photo)}}" alt="">
                                </div>
                                <div class="txtWrp">
                                <h3>{{$s->title}}</h3>
                                    <p>
                                        <strong>{{$s->price}}</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/2.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy Z Fold4 512GB</h3>
                                    <p>
                                        <strong>$1899</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/3.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy Z Flip4 256GB</h3>
                                    <p>
                                        <strong>$1099</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/4.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy S22 8GB 128GB</h3>
                                    <p>
                                        <strong>$849</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>

            <!-- <div class="row mt-4">
                <div class="col-sm-3">
                    <a href="#">
                        <div class="deviceBox">
                            <div class="imgWrp">
                                <img src="{{asset('assets/img/samsung/5.png')}}" alt="">
                            </div>
                            <div class="txtWrp">
                                <h3>Samsung Galaxy S22 8GB 256GB</h3>
                                <p>
                                    <strong>$899</strong> one time-fee
                                </p>
                                <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                    View Details
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="">
                        <div class="deviceBox">
                            <div class="imgWrp">
                                <img src="{{asset('assets/img/samsung/6.png')}}" alt="">
                            </div>
                            <div class="txtWrp">
                                <h3>Samsung Galaxy S22 Plus 8GB 128GB</h3>
                                <p>
                                    <strong>$999</strong> one time-fee
                                </p>
                                <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                    View Details
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#">
                        <div class="deviceBox">
                            <div class="imgWrp">
                                <img src="{{asset('assets/img/samsung/7.png')}}" alt="">
                            </div>
                            <div class="txtWrp">
                                <h3>Samsung Galaxy S22 Plus 8GB 256GB</h3>
                                <p>
                                    <strong>$1049</strong> one time-fee
                                </p>
                                <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                    View Details
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#">
                        <div class="deviceBox">
                            <div class="imgWrp">
                                <img src="{{asset('assets/img/samsung/8.png')}}" alt="">
                            </div>
                            <div class="txtWrp">
                                <h3>Samsung Galaxy S22 Ultra 12GB 256GB</h3>
                                <p>
                                    <strong>$1299</strong> one time-fee
                                </p>
                                <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                    View Details
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div> -->
        </div>
        </div>
    </div>

    <!-- End Section Samsung -->
@endsection