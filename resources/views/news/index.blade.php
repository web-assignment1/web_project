@extends('layouts.master')

@section('content')

<div class="container news-wrapper my-5 specialoffer-font">
    @foreach($news as $i)
        <div class="card my-2" style="width: 18rem;">
            <img src="{{asset($i->photo)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title" style="font-weight:600; font-size:16px; height:5em;">{{$i->title}}</h5>
                <p class="card-text news-wrapper-p">{{$i->description}}</p>
                <a href="#" class="btn news-a">View more</a>
            </div>
        </div>
    @endforeach
</div>
@endsection