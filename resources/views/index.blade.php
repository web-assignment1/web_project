@extends('layouts.master')
@section('content')
@section('slide')
    <!-- Section Slides -->
    <div class="slide-container">
        <div class="slide-box">
            <div class="slide" style="background-image:  url('assets/img/slides/a.webp')"></div>
            <div class="slide" style="background-image:  url('assets/img/slides/b.jpg')"></div>
            <div class="slide" style="background-image:  url('assets/img/slides/c.jpg')"></div>
            <div class="slide" style="background-image:  url('assets/img/slides/4.jpg')"></div>
            <div class="slide" style="background-image:  url('assets/img/slides/5.jpg')"></div>
            <div class="slide" style="background-image:  url('assets/img/slides/6.jpg')"></div>
            <div class="btn-slide btn-back">
                <div class="fas fa-chevron-left"></div>
            </div>
            <div class="btn-slide btn-next">
                <div class="fas fa-chevron-right"></div>
            </div>
            <div class="page-slide">
                <ul>
                
                </ul>
            </div>
        </div>
    </div>
    <h1 align="center" class="title_service">
        <div class="container">
            <marquee behavior="" direction="">Ary Store Phone Shop, available for Sale | Buy | Installment for all kinds of new and used phones, tablets, smart watches, electronic devices, accessories and computers.</marquee>
        </div>
    </h1>
    <!-- End Slide Section -->

    <!-- Section iPhone -->
    <div class="container body-box_iphone mb-4">
        <div class="row mb-2">
            <div class="row">
                <div class="col-sm-2">
                    <div class="image_product">
                        <img src="{{asset('assets/img/iphone.jpg')}}" alt="" >
                    </div>
                </div>
                <div class="col-12 col-md-10">
                    <div class="link_title">
                        <h2><a href="{{route('all_phone.index')}}">iPhone</a></h2>
                    </div>
                    <div class="row mt-4">
                        @foreach($iphone as $i)
                        <div class="col-sm-3 Wrp_product">
                            <a href="{{url('details/iphone?id='. $i->id)}}">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset($i->photo)}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                    <h3>{{$i->title}}</h3>
                                        <p>
                                            <strong>{{$i->price}}</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        <!-- <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/2.jpg')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Pro 256G</h3>
                                        <p>
                                            <strong>$1500</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/3.png')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Pro 512G</h3>
                                        <p>
                                            <strong>$1741</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/4.jpg')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Mini 64G</h3>
                                        <p>
                                            <strong>$639</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                    <!-- <div class="row mt-4">
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/5.png')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Mini 128G</h3>
                                        <p>
                                            <strong>$669</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/8.jpg')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Pro 512G</h3>
                                        <p>
                                            <strong>$1620</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/9.jpg')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Pro 128G</h3>
                                        <p>
                                            <strong>$1258</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/iphone/8.jpg')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>iPhone 12 Mini 128G</h3>
                                        <p>
                                            <strong>$819</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Section iPhone -->

    <!-- Section Samsung -->
    <div class="container body-box_samsung mb-4">
        <div class="row mb-3">
            <div class="row">
                <div class="col-sm-2">
                    <div class="image_product_samsung">
                        <img src="{{asset('assets/img/samsung.jpg')}}" alt="">
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="link_title">
                            <h2><a href="{{route('all_samsung.index')}}">Samsung</a></h2>
                        </div>
                    </div>
                    <div class="row mt-2">
                        @foreach($samsung as $s)
                        <div class="col-sm-3 Wrp_product">
                            <a href="{{url('details/samsung?id='. $s->id)}}">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset($s->photo)}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                    <h3>{{$s->title}}</h3>
                                        <p>
                                            <strong>{{$s->price}}</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                        <!-- <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/samsung/2.png')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>Samsung Galaxy Z Fold4 512GB</h3>
                                        <p>
                                            <strong>$1899</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div> -->
                        <!-- <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/samsung/3.png')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>Samsung Galaxy Z Flip4 256GB</h3>
                                        <p>
                                            <strong>$1099</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-3">
                            <a href="#">
                                <div class="deviceBox">
                                    <div class="imgWrp">
                                        <img src="{{asset('assets/img/samsung/4.png')}}" alt="">
                                    </div>
                                    <div class="txtWrp">
                                        <h3>Samsung Galaxy S22 8GB 128GB</h3>
                                        <p>
                                            <strong>$849</strong> one time-fee
                                        </p>
                                        <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                            View Details
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                </div>
                <!-- <div class="row mt-4">
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/5.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy S22 8GB 256GB</h3>
                                    <p>
                                        <strong>$899</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/6.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy S22 Plus 8GB 128GB</h3>
                                    <p>
                                        <strong>$999</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/7.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy S22 Plus 8GB 256GB</h3>
                                    <p>
                                        <strong>$1049</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/samsung/8.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Samsung Galaxy S22 Ultra 12GB 256GB</h3>
                                    <p>
                                        <strong>$1299</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
            </div>
        </div>
    </div>
    <!-- End Section Samsung -->

    <!-- Section Vivo -->
    <div class="container mb-4">
        <div class="row">
            <div class="col-sm-6">
                <div class="row body-box1">
                    <div class="link_title_box">
                        <h2>Vivo</h2>
                    </div>
                    @foreach($vivo as $v)
                    <div class="col-sm-3">
                        <a href="{{url('details/vivo?id='. $v->id)}}">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset($v->photo)}}" alt="">
                                </div>
                                <div class="txtWrp">
                                <h3>{{$v->title}}</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">{{$v->price}}</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/2.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>vivo V23e 256GB</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$359</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/3.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Vivo Y20s</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$199</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/4.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Vivo Y50</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$249</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row body-box2">
                    <div class="link_title_box">
                        <h2>Oppo</h2>
                    </div>
                    @foreach($oppo as $o)
                    <div class="col-sm-3">
                        <a href="{{url('details/oppo?id='. $o->id)}}">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset($o->photo)}}" alt="">
                                </div>
                                <div class="txtWrp">
                                <h3>{{$o->title}}</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">{{$o->price}}</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/oppo/2.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>OPPO Reno7 4G</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$369</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/oppo/3.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>OPPO A96</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$269</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBoxV_O">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/oppo/4.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>Oppo Find X5 Pro</h3>
                                    <p style="font-size: 10px;">
                                        <strong style="font-size: 10px;">$1199</strong> one time-fee
                                    </p>
                                    <span class="btn btn-sm btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bold;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- End Section SmartWatch -->
    <!-- <div class="container-fluent bg-white mb-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div class="team">
                        <h2>Team Member</h2>
                    </div>
                    <div class="row mb-5">
                        <div class="col-12 col-md-4">
                            <div class="profile">
                                <div class="picture">
                                    <img src="{{asset('assets/img/team/eourmon.jpg')}}" alt="">
                                    <div class="title_profile">
                                        <p>
                                            <strong>Eour Mon</strong><br>
                                            Software Developer 
                                        </p>
                                    </div>
                                    <div class="team-social text-center mb-4">
                                        <ul>
                                            <li>
                                            <a href="#"><i class="bi bi-facebook"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-twitter"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-instagram"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-line"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="profile">
                                <div class="picture">
                                    <img src="{{asset('assets/img/team/team.jpg')}}" alt="">
                                </div>
                                <div class="title_profile">
                                    <p>
                                        <strong>Eour Mon</strong><br>
                                        Software Developer 
                                    </p>
                                </div>
                                <div class="team-social text-center mb-4">
                                    <ul>
                                        <li>
                                        <a href="#"><i class="bi bi-facebook"></i></a>
                                        </li>
                                        <li>
                                        <a href="#"><i class="bi bi-twitter"></i></a>
                                        </li>
                                        <li>
                                        <a href="#"><i class="bi bi-instagram"></i></a>
                                        </li>
                                        <li>
                                        <a href="#"><i class="bi bi-line"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="profile">
                                <div class="picture">
                                    <img src="{{asset('assets/img/team/smile.jpg')}}" alt="">
                                </div>
                                <div class="title_profile">
                                        <p>
                                            <strong>Eour Mon</strong><br>
                                             Software Developer 
                                        </p>
                                    </div>
                                    <div class="team-social text-center mb-4">
                                        <ul>
                                            <li>
                                            <a href="#"><i class="bi bi-facebook"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-twitter"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-instagram"></i></a>
                                            </li>
                                            <li>
                                            <a href="#"><i class="bi bi-line"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
@endsection