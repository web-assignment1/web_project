@extends('layouts.master')
@section('content')
<!-- Section iPhone -->
    <div class="container">
        <div class="row body-box_iphon_detail">
            <div class="col-sm-12 mb-4">
                <div class="link_title">
                    <h2>Vivo</h2>
                </div>
                <div class="row mt-4">
                    @foreach($vivo as $v)
                    <div class="col-sm-3 Wrp_product">
                        <a href="{{url('details/vivo?id='. $v->id)}}">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset($v->photo)}}" alt="">
                                </div>
                                <div class="txtWrp">
                                <h3>{{$v->title}}</h3>
                                    <p>
                                        <strong>{{$v->price}}</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/2.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 256G</h3>
                                    <p>
                                        <strong>$1500</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/3.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 512G</h3>
                                    <p>
                                        <strong>$1741</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/4.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 64G</h3>
                                    <p>
                                        <strong>$639</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
                <!-- <div class="row mt-4">
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/5.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 128G</h3>
                                    <p>
                                        <strong>$669</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/6.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 512G</h3>
                                    <p>
                                        <strong>$1620</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/7.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 128G</h3>
                                    <p>
                                        <strong>$1258</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/vivo/8.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 128G</h3>
                                    <p>
                                        <strong>$819</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
    </div>
    <!-- End Section iPhone -->
@endsection