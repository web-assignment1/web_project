@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-sm-6 detail_box1">
            <div class="detail_box1">
                <div class="txt">
                    <h1>Vivo</h1>
                    <p>{{$details->title}}</p>
                </div>
                <table class="payPlan">
                    <thead>
                        <tr>
                            <th colspan="2"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td><strong>{{$details->price}}</strong>
                                <p>one-time fee</p>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row detail_txt">
                <div class="col-5">
                    <div class="detail_txt">
                        <h5>FREE Bronze SIM</h5>
                        <strong>$54</strong>
                    </div>
                </div>
                <div class="col-7">
                    <div class="detail_txt">
                        <h5>FREE SurfLikeCrazy Monthly 4GB</h5>
                        <strong>52 weeks</strong>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-8">
                    <div class="detail_txt">
                        <a href="#" class="btn btn-lg btn-block cta">
                            Find nearest Smart Shop
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 box2">
            <div class="detail_box2">
                <img src="{{asset('assets/img/vivo/details/1.jpg')}}" alt="">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 box2">
            <div class="detail_box2">
                <img src="{{asset($details->photo)}}" alt="">
            </div>
        </div>
        <div class="col-sm-6 detail_box1">
            <div class="detail_box1">
                <div class="description">
                    <h1>Description</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="des_txt">
                        <table class="table table-borderless text-white">
                            <tbody>
                                <tr>
                                    <td>Screen Size:</td>
                                    <td>6.1 inches, 90.2 cm2 (86.0% screen-to-body ratio)</td>
                                </tr>
                                <tr>
                                    <td>OS:</td>
                                    <td>iOS 16</td>
                                </tr>
                                <tr>
                                    <td>CPU:</td>
                                    <td>A15 Bionic chip</td>
                                </tr>
                                <tr>
                                    <td>Memory:</td>
                                    <td>6GB</td>
                                </tr>
                                <tr>
                                    <td>Camera:</td>
                                    <td>Primary Dual 12MP, Front 12MP</td>
                                </tr>
                                <tr>
                                    <td>Battery:</td>
                                    <td>3279 mAh</td>
                                </tr>
                                <tr>
                                    <td>SIM:</td>
                                    <td>Nano-SIM + eSIM</td>
                                </tr>
                                <tr>
                                    <td>Network:</td>
                                    <td>5G, 4G VoLTE</td>
                                </tr>
                                <tr>
                                    <td>SAR</td>
                                    <td>1.15 W/kg (head)     1.07 W/kg (body)</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection