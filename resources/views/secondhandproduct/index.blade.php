@extends('layouts.master')
@section('content')

<div class="container p-5">

    <div class="secondhand-wrapper">
        @foreach($secondhand as $i)
    	<div class="secondhand-wrapper-item col text-center">
            <a href="{{url('details/secondhand?id='. $i->id)}}">
                <div class="secondhand-wrapper-img">
                    <img src="{{asset($i->photo)}}" style="width:100%; height:100%; object-fit:contain;">
                </div>
                <div class="mt-4">{{$i->title}}</div>
                <div class="secondhand-price">{{$i->price}}</div>
            </a>
        </div>
        @endforeach
    </div>

</div>

@endsection