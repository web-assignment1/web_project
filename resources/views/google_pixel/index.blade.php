@extends('layouts.master')
@section('content')
    <!-- Section iPhone -->
    <div class="container">
        <div class="row body-box_iphon_detail">
            <div class="col-sm-12 mb-4">
                <div class="link_title">
                    <h2 style="color:#c14794;">Google Pixel</h2>
                </div>
                <div class="row mt-4">
                    @foreach($pixel as $i)
                    <div class="col-sm-3 Wrp_product">
                        <a href="{{url('details/iphone?id='. $i->id)}}">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset($i->photo)}}" alt="">
                                </div>
                                <div class="txtWrp">
                                <h3>{{$i->title}}</h3>
                                    <p>
                                        <strong>{{$i->price}}</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/2.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 256G</h3>
                                    <p>
                                        <strong>$1500</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/3.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 512G</h3>
                                    <p>
                                        <strong>$1741</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/4.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 64G</h3>
                                    <p>
                                        <strong>$639</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
                <!-- <div class="row mt-4">
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/5.png')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 128G</h3>
                                    <p>
                                        <strong>$669</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/8.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 512G</h3>
                                    <p>
                                        <strong>$1620</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/9.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Pro 128G</h3>
                                    <p>
                                        <strong>$1258</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a href="#">
                            <div class="deviceBox">
                                <div class="imgWrp">
                                    <img src="{{asset('assets/img/iphone/8.jpg')}}" alt="">
                                </div>
                                <div class="txtWrp">
                                    <h3>iPhone 12 Mini 128G</h3>
                                    <p>
                                        <strong>$819</strong> one time-fee
                                    </p>
                                    <span class="btn btn-lg btn-warning button-details" style="width: 100%; !important; color: #ffffff; font-weight: bolder;">
                                        View Details
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
    </div>
    <!-- End Section iPhone -->
@endsection

<!-- @extends('layouts.master')

<div class="container">
    <div class="row">
        <div class="col position-relative">
            <div class="position-absolute top-50 start-50 translate-middle">
                <h2>New</h2>
                <h2>Pixel 7</h2>
                <h2>Pixel 7 Pro</h2>
                <h5>Starting at $599</h4>
            </div>
        </div>
        <div class="col">
            <img class="" src="{{asset('assets/img/pixel7.webp')}}" style="width:35em;">
        </div>   
    </div>
</div>

@section('content')

<div class="container mt-5">
    <div class="text-center px-5 py-5" style="background-color:white;">
        <h1><b>Latest Products</b></h1>
        <hr style="color:#c14794;">
        <div style="display:flex; justify-content:center;">
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <h5>Pixel 7 Pro</h5>
                <h5>$899</h5>
            </span>
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <h5>Pixel 7 Pro</h5>
                <h5>$899</h5>
            </span>
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <h5>Pixel 7 Pro</h5>
                <h5>$899</h5>
            </span>
        </div>
    </div>

    <div class="text-center mt-5 py-5" style="background-color:white;">
        <h1><b>Most Popular Products</b></h1>
        <hr style="color:#c14794;">
        <div style="display:flex; justify-content:center;">
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <div class="smallHeading">Pixel 7 Pro</div>
                <div class="smallHeading">$899</div>
            </span>
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <div class="smallHeading">Pixel 7 Pro</div>
                <div class="smallHeading">$899</div>
            </span>
            <span class="card text-center" style="width:13rem; border-radius:2em;">
                <img src="{{asset('assets/img/pixel7pro.webp')}}">
                <div class="smallHeading">Pixel 7 Pro</div>
                <div class="smallHeading">$899</div>
            </span>
        </div>
    </div>

    <div class="d-grid gap-2 col-6 mx-auto">
        <button class="btn btn-primary mb-5 mt-5" style="background-color:#000000;" type="button">View All</button>

    </div>


    <div class="normal-font pt-5" style="background-color:#ffffff">
        <div class="heading text-center mb-5">Why Google Phone?</div>
        <div class="row">
            <div class="col">
                <img src="{{asset('assets/img/google_pixel/google-tensor-chip.png')}}" style="width:100%;">
            </div>
            <div class="col position-relative">
                <div class="text-center position-absolute top-50 start-50 translate-middle" style="width:50%;">
                    <div class="normal-font" style="font-size:26px;font-weight:700">
                        Custom-made
                        <br>
                        Google Tensor Chip
                    </div>
                    <div>
                        It makes Pixel fast, efficient, and more secure, and gives you great photo and video quality.
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col position-relative">
                <div class="text-center position-absolute top-50 start-50 translate-middle" style="width:50%;">
                    <div class="normal-font" style="font-size:26px;font-weight:700">
                        An all-around
                        <br>
                        innovative camera.
                    </div>
                    <div>
                        From Real Tone to Magic Eraser to Night Sight, it captures any moment beautifully.
                    </div>
                </div>
            </div>

            <div class="col">
                <img src="{{asset('assets/img/google_pixel/pixel7proCamera.webp')}}" style="width:100%">
            </div>
        </div>

        <div class="row">
            <div class="col">
                <img src="{{asset('assets/img/google_pixel/pixel_sustainability.webp')}}" style="width:100%;">
            </div>
            <div class="col position-relative">
                <div class="text-center position-absolute top-50 start-50 translate-middle" style="width:50%;">
                    <div class="normal-font" style="font-size:26px;font-weight:700">
                        Building products
                        <br>
                        for the planet.
                        <br>
                        And for everyone.
                    </div>
                    <div>
                        From product design to manufacturing and across our supply chains, we are working to address our environmental and social impact at every step.
                    </div>
                </div>
                
            </div>
        </div>
    </div>

</div>

@endsection -->